using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnadirPuntos : MonoBehaviour
{
    // Start is called before the first frame update
    public Movimiento mo;
    int punts;
    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
    }

    private void Update()
    {
        if (mo.FrutaCogi) 
        {
            this.punts += 1;
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Points: " + punts;
            mo.FrutaCogi = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarPunto : MonoBehaviour
{
    public Movimiento mo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mo.vidas == 0) 
        {
            SceneManager.LoadScene(1);
        }
        if (mo.puntuacion == 10)
        {
            SceneManager.LoadScene(2);
        }
    }
}

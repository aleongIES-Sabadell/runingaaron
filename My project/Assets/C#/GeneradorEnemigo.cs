using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigo : MonoBehaviour
{
    public List<GameObject> ObjeCrea;
    float TEspera =10;
    public Movimiento mo;
    // Start is called before the first frame update
    void Start()
    {
        mo.movimiento += al;
        StartCoroutine(Genedar());
    }

    IEnumerator Genedar() 
    {
        while (true) 
        {
            int n = Random.Range(0, ObjeCrea.Count);
            if (n == 0)
            {
                GameObject go = Instantiate(ObjeCrea[0]);
                go.transform.position = new Vector2(10, Random.Range(0, 5));
                go.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, go.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (n == 1)
            {
                GameObject go = Instantiate(ObjeCrea[1]);
                go.transform.position = new Vector2(-10, Random.Range(-5, 0));
                go.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, go.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                GameObject go = Instantiate(ObjeCrea[2]);
                go.transform.position = new Vector2(10, Random.Range(-5, 0));
                go.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, go.GetComponent<Rigidbody2D>().velocity.y);
            }
            Debug.Log(TEspera);
            yield return new WaitForSeconds(TEspera);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void al()
    {
        if (5 < TEspera)
        {
            TEspera -= 0.5f;
        }
    }
}

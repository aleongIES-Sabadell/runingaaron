using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorFruta : MonoBehaviour
{
    public List<GameObject> ObjeCrea;
    public int TEsperaMin;
    public int TEsperaMax;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Genedar());
    }

    IEnumerator Genedar()
    {
        while (true)
        {
            int TE = Random.Range(TEsperaMin, TEsperaMax);
                GameObject go = Instantiate(ObjeCrea[0]);
                go.transform.position = new Vector2(10, Random.Range(-5, 0));
                go.GetComponent<Rigidbody2D>().velocity = new Vector2(-3, go.GetComponent<Rigidbody2D>().velocity.y);
            yield return new WaitForSeconds(TE);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

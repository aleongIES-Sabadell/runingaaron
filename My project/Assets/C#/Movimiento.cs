using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{
    public GameObject go;
    public int vel;
    bool suel;
    public int vidas=3;
    public int puntuacion=0;
    public bool FrutaCogi;
    public delegate void MovimientoDelegate();
    public event MovimientoDelegate movimiento;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Rigidbody2D>().rotation = 0;
        }
        else if (Input.GetKey(KeyCode.A))
        {
           this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Rigidbody2D>().rotation = 0;
        }
        else { 
           this.GetComponent<Rigidbody2D>().velocity = new Vector2(0,this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Rigidbody2D>().rotation = 0;
        }
        if ((Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.Escape))&& suel)
        {
              this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 900));
              this.GetComponent<Rigidbody2D>().rotation = 0;
              suel = false;
        }
        if (this.gameObject.transform.position.y < -20|| vidas<=0)
        {
            SceneManager.LoadScene("GameOver");
            Debug.Log("Cazado");
            Destroy(this.gameObject);
            
        }
        if (this.gameObject.transform.position.y < -20 || puntuacion >= 10)
        {
            Debug.Log("Vivo");
            Destroy(this.gameObject);
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Suelo")
        {
            suel = true;
        }
        else if (col.transform.tag == "Fruta") 
        {
            if (this.vidas >= 3)
            {
                puntuacion++;
            }
            else 
            {
                vidas++;
            }
            this.FrutaCogi = true;
            Debug.Log(this.movimiento);
        }
        else
        {
            Debug.Log("Zoro:" + col.gameObject.name);
            vidas--;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Fruta") 
        {
            if (movimiento != null) 
            {
                movimiento.Invoke();
            }
        }
    }

}
